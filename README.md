Materialize theme for Pelican
=============

This repository is a [Pelican](http://getpelican.com) theme using the CSS framework [Materialize](https://materializecss.com/).


# Installation

[Download](https://gitlab.com/Oprax/pelican-materializecss-theme/-/archive/master/pelican-materializecss-theme-master.zip) or [clone](git@gitlab.com:Oprax/pelican-materializecss-theme.git) this repository to your pelican directory.
Then add to `pelicanconf.py` the variable `THEME` with the path of the `pelican-materializecss-theme` directory.

```python
THEME = "pelican-materializecss-theme"
# or with an absolute path
THEME = "/home/user/pelican-materializecss-theme"
```
